<div align="center">
    <img src="https://cdn.freebiesupply.com/logos/large/2x/nodejs-1-logo-png-transparent.png" width="150">
</div>

# NodeJS - Authen with JWT Refresh Token

API for Authentication with JWT Refresh Token

## Table of Contents

- [Prerequisites](#prerequisites)
- [Configuration](#configuration)
- [Installation](#installation)
- [Usage](#usage)
- [Credits](#credits)
- [License](#license)

## Prerequisites

* Express.js : 4.18.2
* Node : 20.2.0
* Package Manager (npm) : 9.6.6

## Configuration

* Port : 3000

## Installation

Using npm:

```bash
npm install
```

Using docker:

```bash
docker compose up -d
```

## Usage

Using npm:

```bash
npm run start
```

Using docker:

```bash
docker ps --filter name=nodejs_authen_with_jwt_refresh_token_api
```

## Credits

Itsara Rakchanthuek

## License

[MIT](LICENSE)