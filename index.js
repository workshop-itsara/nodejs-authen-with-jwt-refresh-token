const appModule = require('./modules/app/app.module');
const authModule = require('./modules/auth/auth.module');
const express = require('express');
require('dotenv').config();

const app = express();
app.use(express.json());
app.use(appModule.route);
app.use(authModule.route);

app.listen(process.env.APP_PORT, () => {
    console.log(`App running on port ${process.env.APP_PORT}`);
});