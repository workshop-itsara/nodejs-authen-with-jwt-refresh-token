FROM node:20-alpine AS development
WORKDIR /usr/src/app
COPY package*.json .
RUN npm i
COPY . .

FROM node:20-alpine AS production
COPY --from=development /usr/src/app/node_modules ./node_modules
COPY . .
CMD ["npm", "run", "start:prod"]