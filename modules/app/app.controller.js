const appService = require('./app.service');

class AppController {
    constructor() {}

    getApp(req, res) {
        const data = appService.getApp();
        res.send(data);
    }
}

module.exports = new AppController();