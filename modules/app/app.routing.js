const appController = require('./app.controller');
const express = require('express');
let router = express.Router();
let prefix = '';

router.get(`${prefix}/get-app`, appController.getApp);

module.exports = router;