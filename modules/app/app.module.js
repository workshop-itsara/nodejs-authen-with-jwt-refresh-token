const route = require('./app.routing');
const appController = require('./app.controller');
const appService = require('./app.service');

module.exports = {
    route,
    appController,
    appService
};