const jwt = require('jsonwebtoken');
require('dotenv').config();

class AuthService {
    users = [{
            id: 1,
            username: 'tony.stark',
            password: 'marvel',
            name: 'Tony Stark',
            refresh: ''
        },
        {
            id: 2,
            username: 'steve.rogers',
            password: 'marvel',
            name: 'Steve Rogers',
            refresh: ''
        },
        {
            id: 3,
            username: 'thor',
            password: 'marvel',
            name: 'Thor',
            refresh: ''
        },
        {
            id: 4,
            username: 'bruce.banner',
            password: 'marvel',
            name: 'Bruce Banner',
            refresh: ''
        },
        {
            id: 5,
            username: 'natasha.romanoff',
            password: 'marvel',
            name: 'Natasha Romanoff',
            refresh: ''
        },
        {
            id: 5,
            username: 'hawkeye',
            password: 'marvel',
            name: 'Hawkeye',
            refresh: ''
        }
    ];

    constructor() {}

    login(username, password) {
        const userIndex = this.users.findIndex((data) => (data.username === username && data.password === password));
        if (userIndex === -1) return false;
        const accessToken = this.generateAccessToken(this.users[userIndex]);
        const refreshToken = this.generateRefreshToken(this.users[userIndex]);
        this.users[userIndex]['refresh'] = refreshToken;

        return {
            access_token: accessToken,
            refresh_token: refreshToken
        };
    }

    refreshToken(id, token) {
        const userIndex = this.users.findIndex((data) => (data.id === id && data.refresh === token));
        if (userIndex === -1) return false;
        const accessToken = this.generateAccessToken(this.users[userIndex]);
        const refreshToken = this.generateRefreshToken(this.users[userIndex]);
        this.users[userIndex]['refresh'] = refreshToken;

        return {
            access_token: accessToken,
            refresh_token: refreshToken
        };
    }

    generateAccessToken(user) {
        return jwt.sign({
            id: user.id,
            name: user.name
        }, process.env.ACCESS_TOKEN_SECRET, {
            expiresIn: '1m',
            algorithm: 'HS256'
        });
    }

    generateRefreshToken(user) {
        return jwt.sign({
            id: user.id,
            name: user.name
        }, process.env.REFRESH_TOKEN_SECRET, {
            expiresIn: '2m',
            algorithm: 'HS256'
        });
    }

    jwtAccessTokenValidate(req, res, next) {
        try {
            if (!req.headers['authorization']) return res.sendStatus(401);
            const token = req.headers['authorization'].replace('Bearer ', '');

            jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err) => {
                if (err) throw new Error(err);
            });

            next();
        } catch (err) {
            return res.sendStatus(403);
        }
    }

    jwtRefreshTokenValidate(req, res, next) {
        try {
            if (!req.headers['authorization']) return res.sendStatus(401);
            const token = req.headers['authorization'].replace('Bearer ', '');

            jwt.verify(token, process.env.REFRESH_TOKEN_SECRET, (err, decoded) => {
                if (err) throw new Error(err);
                req.user = decoded;
                req.user.token = token;
            });

            next();
        } catch (err) {
            return res.sendStatus(403);
        }
    }

    getUsers() {
        const user = [];

        this.users.forEach((data) => {
            user.push({
                id: data['id'],
                name: data['name']
            });
        });

        return user;
    }
}

module.exports = new AuthService();