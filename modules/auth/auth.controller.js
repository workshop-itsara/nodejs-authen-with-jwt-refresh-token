const authService = require('./auth.service');

class AuthController {
    constructor() {}

    login(req, res) {
        const {
            username,
            password
        } = req.body;
        const data = authService.login(username, password);
        if (!data) return res.sendStatus(401);
        res.json(data);
    }

    refreshToken(req, res) {
        const {
            id,
            token
        } = req.user;
        const data = authService.refreshToken(id, token);
        if (!data) return res.sendStatus(401);
        res.json(data);
    }

    getUsers(req, res) {
        const data = authService.getUsers();
        res.json(data);
    }
}

module.exports = new AuthController();