const authController = require('./auth.controller');
const authService = require('./auth.service');
const express = require('express');
let router = express.Router();
let prefix = '/auth';

router.post(`${prefix}/login`, authController.login);
router.get(`${prefix}/refresh-token`, authService.jwtRefreshTokenValidate, authController.refreshToken);
router.get(`${prefix}/get-users`, authService.jwtAccessTokenValidate, authController.getUsers);

module.exports = router;