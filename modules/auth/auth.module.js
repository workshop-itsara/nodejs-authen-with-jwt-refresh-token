const route = require('./auth.routing');
const authController = require('./auth.controller');
const authService = require('./auth.service');

module.exports = {
    route,
    authController,
    authService
};